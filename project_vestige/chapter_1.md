*When the anomaly was detected, it was a complete accident. An exploration drone malfunctioned and wandered off course, and over the span of a few decades, ended up light years away from its intended destination. It crash landed on Elaria, a class C planet. It was explored nearly two centuries prior, but due to the hazardous surface conditions and hostile wildlife, it was deemed not suitable for colonization by the scout team. By chance, the drone crashed a few miles from the epicenter of the anomaly. The readings triggered the drone’s emergency beacons.*

--- *Notes Trelion, First Science Officer of the Nova expedition*

# Chapter 1: An Unexpected Meeting
## Quinn

The dim neon lights cast a reddish glow on the masked figure seated across the table. The figure was dressed in a dark cloak, obfuscating its shape. A metal box, no larger than a book, was placed squarely in the center. The room was in a rundown taproom on the outskirts of town, nearly devoid of patrons. A persistent electric hum filled the room, creating a static tension in the air. Quinn sat across from the mysterious figure, wearing a dark jacket and utility pants, her dark hair pulled back in a short ponytail. They sat in the far corner of the bar, away from prying eyes and ears.

Quinn templed her fingers and regarded the box with a frown. This was not her informant. What happened to Erren? This person, a Hyrac, judging by the veil covering its face, just shows up with the delivery out of the blue? How did they know the time and place of the meeting? No doubt Erren was dead, and this opportunist was in the right place at the right time. She’d heard Hyracs could glean information from the dead. Regardless of what had happened, if this truly was what she was looking for, she could not pass on the opportunity. The past year of her life had gone into preparing for this moment. She was not about to let anything get in the way of success. The shield engine strapped to her wrist was at least reassuring if the situation went sideways.

Quinn looked at the Hyrac and said, “You’re not Erren.”

“No.” The Hyrac’s deep, garbled voice rumbled from behind the mask, which was a simple black length of cloth wrapped around its head, covering all but the eyes. Such inhuman eyes, she thought. Black, without a hint of emotion.

“What happened to him?” She could see the Hyrac twitched slightly at the question.

“He is dead.”

“How?” She tensed for the answer, and her hand crept towards the stunner at her hip. It probably wouldn’t kill the thing, but it would probably immobilize it long enough for her to grab the box and escape.

“I know not. His corpse was cold when I found it two kilometers east of the city. Vacsuit was punctured. I found the box in his hands.” The Hyrac shifted in its seat, as if expecting what the next question would be.

*So how did you know to bring the box here*? Quinn wanted to ask it, but she couldn’t. She wasn’t sure she’d like the answer. From the stories, the Hyracian method of extracting information from the recently deceased was not a pleasant process. It didn't matter, anyways. She'd worked too hard to give up now. No matter the cost, she would see it to the end. Instead, she asked, “Are you sure the item was recovered without damage? I’m afraid the payment will be forfeit if there is so much as a hint of corrosion.”

“Judge for yourself.” The Hyrac opened the box with gloved hands and pushed it towards her.

Inside the box, hovering within a suspense field, was a small dagger. The straight steel of the blade was only three, perhaps three and a half inches in length. The polished metal cast pinpricks of red on the ceiling. The hilt was wrapped in black leather, and was worn with age and use.

Quinn's heart sank. The dagger was free from corrosion, and certainly looked old. But this was not her dagger. The blade should've been longer, with a slight curve to it, and the leather should've been a dark brown. Most importantly, it was missing the maker's mark etched into the pommel, three intersecting tick marks encompassed by a circle.

*Damn it all*. This wasn't right. She was sure that her informant had been correct. Erren *had* the dagger. Her father's dagger. She could only assume that someone else was aware of the dagger's significance, killed Erren, and swapped the real dagger out for a fake. But if that were the case, why even use a fake? Perhaps they hadn't counted on her having intimate knowledge of the dagger's visual description. Her larger concern now was her new enemy. If the Paragon had discovered Erren's betrayal, this would not go well for her. No, this was far too subtle for them. If the Paragon was involved, she'd already be dead. But who, then, could have known about the dagger? She was not sure she'd like the answer.

Quinn sighed and closed the box. "I'm sorry, but that's not it."

The Hyrac cocked its head. "What do you mean? Is this not the item you hired that courier to retrieve?"

She gave a noncommittal shrug. "It is. But its not what I was looking for. I'm sorry you went through all that trouble for nothing."

"The agreement was one thousand Gen for this box. I have brought it here, and so the reward will be paid." It's voice began to rise in volume, and the few patrons in the bar stared on at the scene.

Huh, that sounded like a threat. Hyracs had such odd notions about fulfilling contracts. She raised her hands in a placating gesture. "Look, you're not even who I was supposed to meet. If you think you can come in here and tell me my informant was killed and you just happened to be in the right place at the right time… Well, I have to say, that sounds a little suspicious."

The Hyrac stood, and she was surprised that it stood nearly two and a half meters tall, menacing in it’s dark cloak. Quinn thought it almost looked like the visage of death itself. "The contract has been fulfilled," it said simply. By now, a few of the taproom’s patrons were making a swift exit. It was not unusual for fights to break out in this part of the city.

*This is not going well*. "Alright, alright. Sit down, big guy, and we can work this out." She pulled out a small black data drive from her pocket and placed it on the table. "A thousand Gen, per the original agreement. But before we go our separate ways, can you at least answer a few questions about what you saw?"

The Hyrac returned to its seat and folded its arms across its chest. "What do you want to know?"

"Well," she began hesitantly. "I know your kind has a… unique ability to… see the last few moments of someone's life. Would you be willing to tell me what you saw when you looked into Erren's mind?"

After a moment of silence, the Hyrac said, "He did not see his death coming. He had no fear. In his last moments, he felt only a quiet confidence and excitement. Then, there was pain. I saw him look down and see the blade sprouting from his chest. As he fell, I saw him glance at his assailant. The vacuit they were wearing… was not standard issue."

So he was murdered. The gears in her mind began to turn. If the suit wasn’t standard issue, then how did the killer get it? She knew some of the higher-ups within the Paragon, including the Paragon’s elite Vakeeran Hunters, were often given special permissions. "The killer. Were they wearing a Hunter's shroud over their suit?"

"It did not appear so."

If it wasn't a Hunter who killed him, then it probably wasn't the Paragon, she thought. There were too many unknowns to be absolutely sure, though. "What about the weapon? Could you tell what it was?"

The Hyrac thought for a moment, then said, "I believe it was a sword. No more than a half-meter in length, curved blade."

Not a spear. Another point against the Paragon. But if not them, then who? "Is there anything more you can tell me?"

Their eyes met, and Quinn could feel a quiet resentment from the Hyrac. "He felt no fear as he died. His last thoughts were of you, Quinn." A cold chill went through her as it said her name. She could feel the words travel down her spine. She hadn't told the Hyrac her name.

She looked down, unable to maintain eye contact. "I… didn't know him well."

They sat in silence for a moment. Quinn broke free of her thoughts and wrapped her knuckles on the table, producing a hollow, wooden sound. "Well, you've been very helpful, Mister…?"

"You can call me Grixis."

"Alright, Grixis. Thank you, truly. I've one more favor to ask. You wouldn't happen to know the approximate coordinates of Erren's body, would you?"

***


Quinn left the taproom a few minutes later, and unpleasant thoughts rattled around in her head. So many complications to what should have been a straightforward exchange. She had worked for so long to gain the trust of a Paragon employee, and even longer still to convince him to help her retrieve the dagger. Now he was dead, and the dagger was gone.

She scanned the city above as she walked down the sullen street. She could see the faint blue hue of the shield sphere surrounding the city, arcing high above the tallest buildings of the upper city. She was in the lower city now, home to Nova's most irredeemable denizens. In the thirty years that Nova had existed, the more unsavory forms of entertainment had slowly grown in popularity. Taprooms, brothels, gambling rings. The Paragon didn't care, as long as the population remained content. Quinn found it all distasteful. Too many were content to waste away their lives on a toxic rock with no purpose or meaning.

Quinn tapped the terminal behind her ear, initiating a neural link with Landry.

There was a faint chirp, and then his voice, sounding excited, echoed through her head: 

*Well? Did you get it*?

*No, she thought back. Erren’s dead.*

*His voice rose with alarm. What? What happened?*

*A Hyrac named Grixis showed up at the rendezvous. Said he found Erren’s body east of the city with the dagger. But… It wasn't it. The dagger, I mean. The shape was all wrong, and it was missing the mark on the hilt.* 

She passed by various shops, selling anything and everything from low grade artificial meats to biotech that was sure to get your arm blown clean off.

Landry was quiet for a few moments. *I'm sorry, Quinn. I really thought this was it.*

*Erren was murdered, Landry. Stabbed through the chest. Whoever killed him probably has the real dagger.* Quinn turned a corner and began heading towards the eastern airlock.

*You can't seriously be considering going after them. If Erren really was killed for the dagger, it's probably in the hands of the Paragon now.* Quinn could hear the concern beginning to seep into his tone. Sometimes Landry forgot he wasn't her real father.

*His body is still out there, slowly melting. I'm going to go investigate. Maybe something will turn up. I know what you're going to say, so don't.*

*It won’t make a difference, huh?* 

*Afraid not. I have to see this through. As far as it will take me.*

*Just be careful.*

*I will. First sign of trouble and I'm gone. I'll meet you back at the Greenery when I'm done*. She tapped her terminal again to sever the link.

He didn’t understand, she thought. Without the dagger, she'd have nothing. She *needed* it. Landry was right about one thing, though. If the Paragon was behind the attack, she would have to give up. One whiff of a Vestige, and they would have a Hunter tracking down the wielder in no time. And she didn't think they asked politely. It had been years since anyone was given the gift, but she still remembered when the disappearances were more frequent. On the other hand, the dagger had been her father's, and he was already dead. They wouldn't want her, right? Either way, she had to try. So, she made her way towards the eastern airlock.
