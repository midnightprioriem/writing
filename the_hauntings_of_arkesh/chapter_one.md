The morning of her death, Sera came to visit me.

My head, split in two from a night spent with a dozen bottles, was not acting right. Waves of pain crashed upon the shore of my brow. I had drunk myself into a stupor, and my dream-self rebelled against me with a vengeance. But this *was* a cruel punishment. 

I blinked, trying to clear my vision, and her shape smeared above me. Floating tendrils danced in my periphery. Surely I was still dreaming. Fog clouded my mind, a heavy mist that prevented me from moving. I closed my eyes, squeezing them so tight that when I open them again, everything remained black for a moment.

But she was still there.

Her pale face, caught in a halo of ethereal hair, gazed past me, unblinking. The daze of sleep drained out of me all at once, and I sat up, suddenly aware that the hallucination still had not faded. Although I had drank enough last night to drown even the hardiest of sailors, my imagination had never been so clever.

My late wife, on the anniversary of her death, was staring down at me from my ceiling.

I gaped at her, and my head filled with the memories of a half-life, all of the joy, and the sorrow. Tears pooled in my eyes before etching rivers down my cheeks. A wound that cut as deep as any knife reopened, one that I had been trying to close.

Unable---or unwilling---to stop it, a swell of happiness wrapped in grief rose deep within my chest. How long had it been since I remembered the delicate curve of her cheeks, or the soft blue of her eyes? I hadn’t even realized I’d forgotten until seeing her visage floating above me.

I searched for some hint of recognition, some spark of the fire in her eyes, but all I could find was a vague curiosity, as if she were not entirely sure why she was here. She seemed to look past me, to the far horizon that did not exist.