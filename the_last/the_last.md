# The Last

“Why are we here, father?”

The boy, no more than ten, looked up at his father with inquisitive brown eyes. He was dressed in dirty brown robes that were a size too big, and had to shield his eyes from the bright Suns above.
“What is our purpose?”

Callir was also dressed in brown robes, and looked out across the horizon, towards the red mounds in the distance that made up their home. Such tiny pinpricks from a distance, and yet encompassed everything he loved. They carried water from the nearby river, their single source of water. To the right of the village, he could make out the meager crops they had managed this year. The harvest season was so short this year, Callir wondered when the planet's orbit would begin to turn back to their favor. If the summer season became much shorter, he was not sure if the village could survive. 

*Our purpose*. He remembered when he’d asked the same question to his Father. Even a boy Brinn’s age began to understand that the simple life they lived on Arelias was hollow. Callir set his water aside and motioned for his son to do the same. He diverted their path away from the village. “Come with me, Brinn, and I’ll show you.”

The dusty barren landscape stretched out before them. Their feet kicked up a plume of dust that trailed into the cloudless sky. They walked until the Suns had fallen low on the horizon. Yatheer would be preparing supper soon. In the distance, a towering pillar of rock loomed. He could feel Brinn's excitement grow as they drew closer, yet his son remained uncharacteristically reserved. Children of the village where taught no sooner than taking their first steps to remain clear of the Tower. It was a warning more severe than any other. But today, Brinn would learn of the tower’s secret.

The Tower itself stood no more than one hundred feet tall, and was perhaps twenty feet in diameter, in a roughly cylindrical shape, tapering towards the roof. Though it looked man-made, there was no sign of any windows or doors. The outside was perfectly smooth, and was flat gray color. Callir could feel energy from within, the heavy build-up of static pressure, like a silent deafening in one’s ears. The sensation was always the same, and familiar to him now, but utterly alien.

Callir approached the Tower, and Brinn followed apprehensively behind. “…Father, I thought the Tower was forbidden.”

“It is,” Callir said simply. “And for good reason. But, you have asked the question that I asked my Father, and he his, and each of our Ancestors before us. And it is for that reason I have brought you here. In the Tower lies our purpose. We are more than just simple barren farmers, Brinn. Come.”

Callir placed his hand on the wall, and murmured the words. “*Urieth alrvuren vulternas ireth nencurath.*” A seam appeared in the stone and parted, opening an unseen doorway. He felt a cool, dry breeze wick moisture from his brow. Inside the Tower was dark, but when they entered, a dim, warm light began to emanate from all around. To Brinn’s surprise, a set of stairs took them down, not up, as one would expect from the outward appearance.

The uneven staircase wound down for what felt like hours, and Brinn began to tire. Still, he would push on. He could tell from his father's expression that what they were doing was important. He would not let his father down.

"We're almost there," Callir said. "Not much farther now."

Eventually they reached the bottom of the stairs. Callir led them down a long hallway. The warm light still followed, illuminating only a few meters in front and behind them. The pair came to a heavy stone door at the end of the hallway. Callir pushed, and with a grunt of effort, opened the door. 

Before walking through, he turned to his son. "Brinn, what I am about to show you is sacred to the Arelian people. What lies beyond this door is why we are hear. I tell you this so that you understand the gravity of our situation. When I die, this task will pass to you."

Brinn stared up at his father. The usual laugh lines in his face, eyes crinkled with happiness, were replaced with solemn features. He took a deep breath, and said, "I'm ready, father. I want to know our purpose."

They walked in together, and the Brinn's eyes grew wide. Before them was some kind of viewing balcony, surrounded by a metallic barrier. Approaching the barrier, Brinn could see that the space beyond descended into darkness, beyond the scope of his vision. For all he knew, it went all the way to the center of the planet. The room itself was circular, and topped with a concave dome, and was as wide as the span of their entire village. Suspended in the center of the space, horizontal to their viewing area, was a massive mechanical sphere. Numerous rings orbited the sphere at varying angles and speeds, and it emitted an audible hum that vibrated the chamber. There did not seem to be any structure that supported the sphere. It hung, unwavering, as the rings continued to spin, as if it had been since the beginning of time.

Brinn was in awe. The sheer size of the sphere alone was daunting. More than that, he could sense something within the core of the sphere. An energy, or great power, that radiated towards him, and the magnitude of it overwhelmed him.

Callir gripped his son on his shoulders, and Brinn turned to face him, tearing his eyes from the spectacle.

“We are The Last. Our ancestors left Arelias long before it became the barren husk we live on now. But, a few of us had to stay behind. This machine, a Sourcegate, our ancestors called it, is the source of all life in our Universe. It is said that if it ever is destroyed or is shut off, the entire Universe would be thrust into chaos, like a spinning top gone awry. Brinn, your life has a grand purpose. We Arelians protect the Universe.”

