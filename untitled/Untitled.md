There's something you should know about me before we begin---I never set out to become a Demon. Choice is an indulgence for the strong. And I was weak. 

They came in the night, on the eve of my thirtieth year. Scrap Demons that were looking to fill their quotas. I was human then, and while I don't remember the pain they inflicted on me, the holes they left in my memory serve as a sort of calibration point for what happened to me. They performed their rituals, and *inducted* me without little fanfare or preparation. No Contract, no training of any kind. I learned later that the survival rate for spontaneous inductions, as they are so eloquently called, is around three percent. So I guess I'm pretty special then, huh? Yay me.

After my unceremonious induction and some time spent adjusting to my new lifestyle, I was sent to Alacast, a city so smug you might trip over someone's ego walking down the street. A city of grand magi and scholars, as they say. *The Grand Basilica, the finest university west of Vere*---as if adding the word *grand* to something will make it so. The people here are either so far up their own hubris they can't see the light of day, or are otherwise occupied with pedantic debate over countless meaningless theorems and formulae. As if they could ever truly understand the Craft. That was one thing, atleast, that I got out of my deal. A sudden and innate understanding of all magic.

Now, my purpose, as a Demon, that is, is to proliferate our cause. The *right* way to do this---for those keeping score, *not* the way it happened for me---is to sign potential recruits into a Contract. They become an apprentice, and are taught the ways of the Craft, and by way of the Contract, are granted powers beyond what any university or bog-standard wizard might teach. Over the years, they fall closer to our side, and eventually are inducted into the family. Very sweet. 

I had been given my first quota. It comes with the gig. Become a Demon, get a quota. No free lunch and all that. I was to head to Alacast, sign an apprentice, and influence them in *demonic* ways. Demons are straightforward, at least. I'll give them that much. There was only one problem. I didn't ask to become a Demon. I didn't *ask* for this power. Like I'd try telling that to the Demons upstairs, though. No reason to rock the boat if I didn’t have to.  And so to Alacast I went.

---

As I walked down the streets of Alacast, an icy wind snaked between the buildings, filling passersby with a deep chill. One of the perks of being a Demon---I could sense how they felt. It was strange; I could always feel it. If I reached out, the sensations became stronger, more defined somehow, but they were always there. Ever present. 

The feelings and emotions of every human in a mile radius can be overwhelming at best. So much rage, jealously, and fear swirled inside each and every one of them. It gave me a stomach ache.

The spires of the Grand Basilica loomed overhead, an ancient amalgation of stone buildings that housed some of the most pretentious men in Sinamar. It was also a university, predominantly of magic, and the perfect hunting grounds to find my quarry. The Balisica was known across the Arc as *the* definitive place to learn magic. 

Would-be Magi from all over make the pilgrimage to Sinamar each year in hopes of being accepted into the college. The entrance exams were notoriously challenging---only the very best prospects are accepted each year.



